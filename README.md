# linux kernel module

**Laboratorio 3: Desarrollo de módulos para el kernel de Linux**

*Objetivos*

El objetivo de este trabajo práctico es el desarrollo de dos simples módulos
para insertar en el kernel de linux. Estos módulos van a emular el funcionamiento de
dos dispositivos de caracteres. Uno de los dispositivos realizará una encriptación simple
de los caracteres que se escriben en el. El otro de los módulos realizará la
desencriptación de los caracteres que se escriben en el.

*Introducción*

Los sistemas operativos basados en el kernel Linux funcionan correctamente en una gran
variedad de plataformas de hardware, diferentes placas de video, diferentes placas de
red, diferentes discos duros, etc. La mayor parte de las aplicaciones de estos sistemas
operativos son desarrolladas sin conocer las características del hardware sobre el que
serán utilizadas. Es la tarea del kernel Linux la de actuar de interfáz entre las
aplicaciones y las diferentes plataformas de hardware. Gran parte de la tarea de
comunicación entre el kernel y los diferentes dispositivos físicos es realizada por los
controladores de dispositivos o módulos.

La tarea de los módulos es presentar al kernel un conjunto estandarizado y bien definido
de llamadas al sistema, estas llamadas son independientes del tipo de dispositivo. El
módulo luego traduce estas llamadas a las operaciones específicas del hardware para el
que fue diseñado.
Este conjunto de llamadas al sistema esta definida de tal manera que los módulos pueden
ser desarrollados de manera separada del resto del kernel. Estos módulos son cargados
en tiempo de ejecución en el momento en que son necesarios.
